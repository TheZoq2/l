#top = framebuffer::framebuffer_test_harness

from cocotb.clock import Clock
from spade import FallingEdge, SpadeExt
from cocotb import cocotb

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut) # Wrap the dut in the Spade wrapper

    # To access unmangled signals as cocotb values (without the spade wrapping) use
    # <signal_name>_i
    # For cocotb functions like the clock generator, we need a cocotb value
    clk = dut.clk_i

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())

    s.i.frame_flip = "false";
    s.i.rst = "true"
    [await FallingEdge(clk) for _ in range(0, 10)]
    s.i.rst = "false"


    s.i.frame_flip = "true";
    await FallingEdge(clk)
    s.i.frame_flip = "false";

    for _ in range(0, 4000):
        await FallingEdge(clk)

    s.i.frame_flip = "true";
    await FallingEdge(clk)
    s.i.frame_flip = "false";

    for _ in range(0, 4000):
        await FallingEdge(clk)
