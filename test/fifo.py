# top=fifo::testinst
import cocotb
from spade import SpadeExt
from cocotb.clock import Clock
from cocotb.triggers import FallingEdge
from cocotb.triggers import Timer


@cocotb.test()
async def normal_operation(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units='ns').start())
    s.i.rst = "true"

    await FallingEdge(clk)
    s.i.rst = "false"
    s.i.write = "None()"
    s.i.read_enable = "false"
    await Timer(1, units='ps')
    s.o.assert_eq("None()")

    await FallingEdge(clk)
    s.o.assert_eq("None()")

    s.i.read_enable = "false"
    await FallingEdge(clk)
    s.o.assert_eq("None()")

    s.i.write = "Some(5)"
    await Timer(1, units='ps')
    await FallingEdge(clk)
    s.o.assert_eq("None()")

    s.i.write = "Some(2)"
    await Timer(1, units='ps')
    s.o.assert_eq("None()")
    await FallingEdge(clk)

    s.i.write = "None()"
    s.i.read_enable = "true"
    await Timer(1, units='ps')
    s.o.assert_eq("Some(5)")
    await FallingEdge(clk)

    await Timer(1, units='ps')
    s.o.assert_eq("Some(2)")
    await FallingEdge(clk)
    
    await Timer(1, units='ps')
    s.o.assert_eq("None()")
    await FallingEdge(clk)

    await Timer(1, units='ps')
    s.o.assert_eq("None()")
    await FallingEdge(clk)

    s.i.write = "Some(7)"
    await Timer(1, units='ps')
    s.o.assert_eq("None()")
    await FallingEdge(clk)

    s.i.write = "Some(4)"
    s.i.read_enable = "false"
    await Timer(1, units='ps')
    s.o.assert_eq("None()")
    await FallingEdge(clk)

    s.i.write = "None()"
    await Timer(1, units='ps')
    s.o.assert_eq("None()")
    await FallingEdge(clk)
    
    s.i.read_enable = "true"
    await Timer(1, units='ps')
    s.o.assert_eq("Some(7)")
    await FallingEdge(clk)

    await Timer(1, units='ps')
    s.o.assert_eq("Some(4)")
    await FallingEdge(clk)

