use std::ports::read_mut_wire;
use std::ports::new_mut_wire;
use std::ops::div_pow2;
use std::conv::int_to_uint;

// use protocols::uart::uart_tx;

use ulx3s_sdram::main::SdramPins;
use ulx3s_sdram::main::SdramStatus;
use ulx3s_sdram::main::Uart;
use ulx3s_sdram::avalon::AvalonPort;

use hdmi::plls::pll_640_480_60;
use hdmi::plls::pll_1024_600_60 as hdmi_pll;
use hdmi::plls::Clocks;

use mcp3002::adc::adc_driver;
use mcp3002::adc::timing_40mhz;

use lib::ecp5_stubs::iddrx1f;
use lib::ecp5_stubs::pcsclkdiv;
use lib::ecp5_stubs::PcsclkdivOut;

use lib::framebuffer::framebuffer;
use lib::pixel_ram::PixelRamW;
use lib::pixel_ram::pixel_ram;
use lib::hdmi::hdmi_out;
use lib::write_cache::write_cache;
use lib::mem::dp_bram;
use lib::mem::dp_bram_2;
use lib::fifo::bram_fifo;
use lib::fifo::Receiver;
use lib::fifo::Sender;
use lib::snek::SnekSegment;
use lib::snek::Food;
use lib::snek::snek_logic;
use lib::snek::food_logic;

use lib::generated::sdram_controller::sdram_controller;



#[no_mangle]
entity main(
    #[no_mangle] BTN_N: bool, 

    #[no_mangle] gpdi_dp: &mut [bool; 4],


    uart: Uart,

    sdram_a: &mut int<13>,
    sdram_ba: &mut int<2>,
    sdram_ras_n: &mut bool,
    sdram_cas_n: &mut bool,
    sdram_we_n: &mut bool,
    sdram_cs_n: &mut bool,
    sdram_dm: &mut int<2>,
    sdram_dq: &int<16>,
    sdram_cke: &mut bool,
    sdram_clock: &mut bool,

    #[no_mangle] miso: bool,
    #[no_mangle] mosi: &mut bool,
    #[no_mangle] sclk: &mut bool,
    #[no_mangle] cs: &mut bool,

    #[no_mangle] debug_pins: &mut [bool; 8],

    #[no_mangle] clk_25mhz: clock,
) {


    let Clocks$(clk_pixel: vga_clk, clk_shift) = inst hdmi_pll(clk_25mhz);


    let lib::dram_pll::SdramClocks$(clk_50e6, clk_50e6_180deg) =
            inst lib::dram_pll::dram_pll$(clk25: clk_25mhz);
    let dram_clk = clk_50e6;
    reg(dram_clk) rst_count: uint<8> initial(10) = if rst_count == 0 {0} else {trunc(rst_count - 1)};
    let rst = rst_count != 0;

    let (hdmi_avalon0, hdmi_avalon1, cam_avalon0, cam_avalon1, cam_avalon2, status) = inst sdram_controller$(
        clk_50e6: clk_50e6,
        clk_50e6_180deg: clk_50e6_180deg,
        rst,
        uart,
        pins: SdramPins$(
            sdram_a,
            sdram_ba,
            sdram_ras_n,
            sdram_cas_n,
            sdram_we_n,
            sdram_cs_n,
            sdram_dm,
            sdram_dq,
            sdram_cke,
            sdram_clock
        )
    );

    let frame_flip_hdmi = inst(3) hdmi_out$(
        vga_clk,
        clk_shift,
        rst,
        gpdi_dp,
        dram_clk,
        avalons: (hdmi_avalon0, hdmi_avalon1),
    );

    let inputs = inst lib::input::drive_adc$(clk: dram_clk, rst, adc_miso: miso);

    set cs = inputs.adc_pins.cs;
    set mosi = inputs.adc_pins.mosi;
    set sclk = inputs.adc_pins.sclk;

    let (snek1_mem_write, snek1_mem_read1, snek1_mem_read2) = inst dp_bram_2::<10, SnekSegment, 1024>(dram_clk, dram_clk);
    let snek1_head_write = inst new_mut_wire();
    let snek1_head_read = inst read_mut_wire(snek1_head_write);
    let snek1_eat_write = inst new_mut_wire();
    let snek1_eat_read = inst read_mut_wire(snek1_eat_write);
    let (snek1_food_send, snek1_food_recv) = inst bram_fifo::<10, (int<16>, int<16>), 1024>(dram_clk, rst);
    
    let (snek2_mem_write, snek2_mem_read1, snek2_mem_read2) = inst dp_bram_2::<10, SnekSegment, 1024>(dram_clk, dram_clk);
    let snek2_head_write = inst new_mut_wire();
    let snek2_head_read = inst read_mut_wire(snek2_head_write);
    let snek2_eat_write = inst new_mut_wire();
    let snek2_eat_read = inst read_mut_wire(snek2_eat_write);
    let (snek2_food_send, snek2_food_recv) = inst bram_fifo::<10, (int<16>, int<16>), 1024>(dram_clk, rst);

    let (food_mem_write, food_mem_read1) = inst dp_bram::<10, Food, 1024>(dram_clk, dram_clk);

    decl frame_flip;
    let _ = inst snek_logic$(
        clk: dram_clk,
        rst,
        vsync: frame_flip,
        angle: 32,
        this_head: snek1_head_write,
        other_head: snek2_head_read,
        did_eat: snek1_eat_read,
        mem_read: snek1_mem_read1,
        mem_write: snek1_mem_write,
        food_send: snek1_food_send,
        initial_offset: 0,
    );
    let _ = inst snek_logic$(
        clk: dram_clk,
        rst,
        vsync: frame_flip,
        angle: 23,
        this_head: snek2_head_write,
        other_head: snek1_head_read,
        did_eat: snek2_eat_read,
        mem_read: snek2_mem_read1,
        mem_write: snek2_mem_write,
        food_send: snek2_food_send,
        initial_offset: 100,
    );
    let _ = inst food_logic$(
        clk: dram_clk,
        rst,
        vsync: frame_flip,
        head_1: snek1_head_read,
        head_2: snek2_head_read,
        eat_1: snek1_eat_write,
        eat_2: snek2_eat_write,
        food_1: snek1_food_recv,
        food_2: snek2_food_recv,
        mem_read: food_mem_read1,
        mem_write: food_mem_write,
    );

    let frame_flip = match inst lib::cdc::handshake(
        vga_clk,
        rst,
        if frame_flip_hdmi {Some(true)} else {None()},
        dram_clk
    ) {
        Some(true) => true,
        _ => false
    };

    let fb_out = inst(7) framebuffer$(
        clk: dram_clk,
        rst,
        snek_mem0: snek1_mem_read2,
        snek_mem1: snek2_mem_read2,
        write_avalon: cam_avalon2,
        clear_avalon: cam_avalon0,
        frame_flip
    );

    let _ = cam_avalon1.inst unused();

    set debug_pins = [
        frame_flip,
        (!(*cam_avalon0.waitrequest) && inst read_mut_wire(cam_avalon0.write)),
        // (!(*cam_avalon1.waitrequest) && inst read_mut_wire(cam_avalon1.write)),
        inst read_mut_wire(cam_avalon0.address) == 500,
        (!(*cam_avalon2.waitrequest) && inst read_mut_wire(cam_avalon2.write)),
        (!(*hdmi_avalon0.waitrequest) && (inst read_mut_wire(hdmi_avalon0.read) || (*hdmi_avalon0.readdatavalid))),
        (!(*hdmi_avalon1.waitrequest) && (inst read_mut_wire(hdmi_avalon1.read) || (*hdmi_avalon1.readdatavalid))),
        (!(*cam_avalon0.waitrequest) && inst read_mut_wire(cam_avalon0.write))
            || (!(*cam_avalon1.waitrequest) && inst read_mut_wire(cam_avalon1.write))
            || (!(*cam_avalon2.waitrequest) && inst read_mut_wire(cam_avalon2.write))
            || (!(*hdmi_avalon0.waitrequest) && inst read_mut_wire(hdmi_avalon0.read))
            || (!(*hdmi_avalon1.waitrequest) && inst read_mut_wire(hdmi_avalon1.read)),
        fb_out.dbg_current_fb,
    ];
}

